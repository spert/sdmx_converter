﻿using Microsoft.Extensions.Configuration;
using SdmxConverterV2.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SdmxConverterV2.Job
{
    public interface IDbService
    {
        void Run();
    
    }

    public class DbService : IDbService
    {
        IConfiguration _config;
        IFileManager _fmgr;

        public List<Dataset> Datasets { get; set; }

        public DbService(IConfiguration config, IFileManager fmgr)
        {
           _config = config;
           _fmgr = fmgr;
        }

        public void Run()
        {

            DeserializeDatabase();

            Console.WriteLine("Fuck off");

        }

        public void DeserializeDatabase()
        {
            string rootF = _fmgr.GetDatabaseRootFolder();
            string dsFile = _config["Database:DatasetsFile"];

            string dsFilePath = Path.Combine(rootF,dsFile);
            string jsonString = File.ReadAllText(dsFilePath);
            DbRoot database = JsonSerializer.Deserialize<DbRoot> (jsonString);          

        }     

    }

}
