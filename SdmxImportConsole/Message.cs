﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxImportConsole
{
    public abstract class Message
    {
        public abstract string Text { get; set; }
    }

    public class ErrorMessage : Message
    {
        public override string Text { get; set; }
    }

    public class ReturnMessage : Message
    {
        public override string Text { get; set; }
    }
}
