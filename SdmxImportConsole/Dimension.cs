﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxImportConsole
{
    public class Dimension
    {
        string _datasetId;
        string _structureName;
        string _codelist;
        string _description;
        string _conceptRef;
        int _isFrequencyDimension;
        int _isTimeDimension;
        int _isPrimaryMeasure;
        int _isObservationStatus;
        int _isTimeFormat;
        static int _counter;

        public string DatasetId { get { return  _datasetId ; } set { _datasetId = value; } }

        public string StructureName { get { return  _structureName ; } set { _structureName = value; } }

        public string  Codelist { get { return  _codelist ; } set { _codelist = value; } }

        public string Description { get { return  _description ; } set { _description = value; } }

        public string ConceptRef { get { return  _conceptRef ; } set { _conceptRef = value; } }

        public int IsTimeDimension { get { return _isTimeDimension; } set { _isTimeDimension = value; } }

        public int IsFrequencyDimension { get { return _isFrequencyDimension; } set { _isFrequencyDimension = value; } }

        public int IsTimeFormat { get { return _isTimeFormat; } set { _isTimeFormat = value; } }

        public int IsPrimaryMeasure { get { return _isPrimaryMeasure; } set { _isPrimaryMeasure = value; } }

        public int IsObservationStatus { get { return _isObservationStatus; } set { _isObservationStatus = value; } }

        public Dimension(string DatasetId, string StructureName, string Codelist, string Description, string ConceptRef, int IsTimeDimension, int IsFrequencyDimension, int IsTimeFormat, int IsPrimaryMeasure, int IsObservationStatus)
        {
            _datasetId = DatasetId;
            _structureName = StructureName; 
            _codelist = Codelist;
            _description = Description;
            _conceptRef = ConceptRef;
            _isTimeDimension = IsTimeDimension;
            _isFrequencyDimension = IsFrequencyDimension;
            _isTimeFormat = IsTimeFormat;
            _isPrimaryMeasure = IsPrimaryMeasure;
            _isObservationStatus = IsObservationStatus;
        }    

    }

    public sealed class DimensionClassMap : ClassMap<Dimension>
    {
        public DimensionClassMap()
        {
            Map(m => m.DatasetId).ConvertUsing(m => m.DatasetId.InQuotes());
            Map(m => m.StructureName).ConvertUsing(m => m.StructureName.InQuotes());
            Map(m => m.Codelist).ConvertUsing(m => m.Codelist.InQuotes());
            Map(m => m.Description).ConvertUsing(m => m.Description.InQuotes());
            Map(m => m.ConceptRef).ConvertUsing(m => m.ConceptRef.InQuotes());
            Map(m => m.IsTimeDimension).ConvertUsing(m => m.IsTimeDimension.ToString());
            Map(m => m.IsFrequencyDimension).ConvertUsing(m => m.IsFrequencyDimension.ToString());
            Map(m => m.IsTimeFormat).ConvertUsing(m => m.IsTimeFormat.ToString());
            Map(m => m.IsPrimaryMeasure).ConvertUsing(m => m.IsPrimaryMeasure.ToString());
            Map(m => m.IsObservationStatus).ConvertUsing(m => m.IsObservationStatus.ToString());
            Map(m => m.IsTimeFormat).ConvertUsing(m => m.IsTimeFormat.ToString()); ;
        }
    }
}
