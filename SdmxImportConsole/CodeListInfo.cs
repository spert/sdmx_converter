﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxImportConsole
{
    public class CodelistInfo
    {
        public string id;

        public string urn;

        public string structureURL;

        public string agencyId;

        public string version;

        public string name;

        public string description;

        public string language;

        public CodelistInfo()
        {
            this.language = "en";
        }
    }
}
