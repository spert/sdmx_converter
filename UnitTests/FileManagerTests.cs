

using Microsoft.VisualStudio.TestTools.UnitTesting;
using SdmxConverterV2;
using SdmxConverterV2.Job;
using SdmxConverterV2.Util;
using System.Net.Http.Headers;

namespace UnitTests
{
    [TestClass]
    public class FileManagerTests
    {
        //string DatabaseRootFolder = @"C:\Users\kasutaja\source\repos\sdmx_converter\SdmxConverterV2\bin\Debug\net6.0\test\";

        //FileManager fileManager = new FileManager();

        public FileManagerTests()
        {
            //string tableOfContentsDir = DatabaseRootFolder;

            //if (!Directory.Exists(tableOfContentsDir))
            //{
            //    Directory.CreateDirectory(tableOfContentsDir);
            //}
        }


        [TestMethod]
        public void DownloadTableOfContents()
        {            
            string target = Helpers.GetTocFilePath();

            if (!File.Exists(target))
            {
                File.Delete(target);
            }

            fileManager.DownloadFile(source, target);

            Assert.IsTrue(File.Exists(target));

        }

        [TestMethod]
        public void DownloadSdmxZipFile()
        {      
            
            Uri sourcePath = Helpers.GetZipWebAddress("aact_ali01");
            string targetPath = Helpers.GetZipPath("aact_ali01");            

            if (!Directory.Exists(Path.GetDirectoryName(targetPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(targetPath));
            }

            if (File.Exists(targetPath))
            {
                File.Delete(targetPath);
            }

            fileManager.DownloadFile(sourcePath, targetPath);

            Assert.IsTrue(File.Exists(targetPath));

        }

        [TestMethod]
        public void ExtractZipFile()
        {
            string source = Helpers.GetZipPath("aact_ali01");
            string targetFolder = Helpers.GetDatasetFolder("aact_ali01");

            if (!Directory.Exists(targetFolder))
            {
                Directory.CreateDirectory(targetFolder);
            }

            fileManager.ExtractSdmxFile(source, targetFolder);

            Assert.IsTrue(new System.IO.DirectoryInfo(targetFolder).GetFiles("*.xml").Length == 2);

        }

        [TestMethod]
        public void ReadDbStruct()
        {    
            Database db = fileManager.ReadDb();

            Assert.IsNotNull(db);
        }


    }
}