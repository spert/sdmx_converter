﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdmxImportConsole
{
    public class Concept
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _isTimedimension;

        public bool IsTimeDimension
        {
            get { return _isTimedimension; }
            set { _isTimedimension = value; }
        }

        private bool _isPrimaryMeasure;

        public bool IsPrimaryMeasure
        {
            get { return _isPrimaryMeasure; }
            set { _isPrimaryMeasure = value; }
        }

        private bool _isObservationStatus;

        public bool IsObservationStatus
        {
            get { return _isObservationStatus; }
            set { _isObservationStatus = value; }
        }

        private int _counter;

        public int Counter
        {
            get { return _counter; }
            set { _counter = value; }
        } 
    }
}
