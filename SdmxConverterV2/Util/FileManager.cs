﻿using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Extensions.Configuration;
using SdmxConverterV2.Job;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SdmxConverterV2.Util
{
    public interface IFileManager
    {
        void DownloadTableOfContents();
        void DownloadFile(Uri source, string target);
        void ExtractSdmxZipFile(string DatasetId);
        string GetDatabaseRootFolder();

    }

    public class FileManager : IFileManager
    {
        IConfiguration _config;

        public FileManager(IConfiguration Config)
        {
            _config = Config;
        }

        public string GetDatabaseRootFolder()
        {
            return _config["Database:DbRootFolder"];     
        }

        public string GetTocFilePath()
        {
            return Path.Combine(GetDatabaseRootFolder(), "Table_of_contents_en.txt");
        }

        public Uri GetTocWebAddress()
        {
            return new Uri(_config["Database:TocWebAddress"]);
        }

        public Uri GetZipWebAddress(string DatasetId)
        {
            return new Uri(string.Concat(_config["Database:ZipWebAddress"], @"/", DatasetId, ".sdmx.zip"));
        }

        public string GetZipPath(string DatasetId)
        {
            return Path.Combine(GetDatabaseRootFolder(), DatasetId, string.Concat(DatasetId, ".sdmx.zip"));
        }

        public string GetDatasetFolder(string DatasetId)
        {
            return string.Concat(GetDatabaseRootFolder(), @"\", DatasetId);
        }

        public void DownloadTableOfContents()
        {
            Uri tocWeb = GetTocWebAddress();
            string tocFile = GetTocFilePath();

            if (!File.Exists(tocFile))
                File.Delete(tocFile);
                        
            DownloadFile(tocWeb, tocFile);
        }

        public void DownloadSdmxZipFile(string DatasetId)
        {
            Uri sourcePath = GetZipWebAddress(DatasetId);
            string targetPath = GetZipPath(DatasetId);

            if (!Directory.Exists(Path.GetDirectoryName(targetPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(targetPath));

            if (File.Exists(targetPath))
                File.Delete(targetPath);

            DownloadFile(sourcePath, targetPath);

        }

        public void DownloadFile(Uri source, string target)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                wc.DownloadFile(source, target);
            }
        }

        public void ExtractSdmxZipFile(string DatasetId)
        {
            string source = GetZipPath(DatasetId);
            string targetFolder = GetDatasetFolder(DatasetId);

            if (!Directory.Exists(targetFolder))
                Directory.CreateDirectory(targetFolder);

            foreach (var file in new DirectoryInfo(targetFolder).EnumerateFiles("*.xml"))
            {
                file.Delete();
            }

            ExtractFile(source, targetFolder);

        }

        public void ExtractFile(string sourceZipFile, string targetFolder)
        {

            FastZip fastZip = new FastZip();

            fastZip.ExtractZip(sourceZipFile, targetFolder, null);
        
        }     

    }
}
