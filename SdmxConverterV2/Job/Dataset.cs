﻿using SdmxConverterV2.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SdmxConverterV2.Job
{
    public class RootOb
    {

        public RootOb()
        {
            
        }
        public List<Dataset> Database { get; set; }
    
    }

    public class Dataset
    {
        public Dataset()
        {                      
        }

        public string DatasetId { get; set; }

        public string DoUdpate { get; set; }

        public string LastUpdated { get; set; }
   
        public string Description { get; set; }
        
    }
}
