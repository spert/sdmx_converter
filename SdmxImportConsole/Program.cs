﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SdmxImportConsole
{
    class Program
    {
        static List<HeaderFile> _filesToDownload = new List<HeaderFile>();

        static List<string[]> csvUpdateFile = new List<string[]>();

        static ConcurrentBag<Message> outputMessages = new ConcurrentBag<Message>();

        static int Main(string[] args)
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, bargs) =>
            {
                String dllName = new AssemblyName(bargs.Name).Name + ".dll";
                var assem = Assembly.GetExecutingAssembly();
                String resourceName = assem.GetManifestResourceNames().FirstOrDefault(rn => rn.EndsWith(dllName));
                if (resourceName == null) return null;
                using (var stream = assem.GetManifestResourceStream(resourceName))
                {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };

            if (args.Length == 0)
            {
               var sett = AppDomain.CurrentDomain.BaseDirectory + @"FilesToDownload.txt";

                if (File.Exists(sett))
                {
                    args = new string[1];
                    args[0] = AppDomain.CurrentDomain.BaseDirectory + @"FilesToDownload.txt";
                }
            }

            // ========= Read settings
            var mess = SettingsFileReader(args);

            if (mess is ErrorMessage)
            {
                Console.WriteLine(mess.Text);
                Console.ReadLine();
                return 1;
            }

            Console.WriteLine("Importing data! Wait while processing large datsets ...");
            Console.WriteLine("");

            // =========== Whitch ones should be updated
            FilesNeedUpdating();

            // ========== File conversion
            Task ret = SdmxFileConversionLoop();
            ret.Wait();

            // ========== Print results
            bool error = false;
            foreach (var t in _filesToDownload)
            {
                if (t.Message is ErrorMessage)
                {
                    error = true;
                }
            }

            if (error)
                Console.ReadLine();

            Console.WriteLine("");
            Console.WriteLine("Importing finished");

            return 0;

        }

        static void FilesNeedUpdating()
        {

            var messUpd = Downloader.DownloadUpdatesFile();

            if (messUpd is ErrorMessage)
                return;

            // new update file
            var csvUpdateFile = File.ReadAllLines(messUpd.Text).Select(a => a.Split('\t'));

            foreach (HeaderFile myFile in _filesToDownload)
            {
                DateTime? oldUpdate = null;

                if (!File.Exists(myFile.HeaderFilePath))
                {
                    myFile.Update = true;
                    oldUpdate = DateTime.MinValue;
                }
                else
                {
                    var headerFile = File.ReadAllLines(myFile.HeaderFilePath).Select(a => a.Split(';')).ToList();

                    if (headerFile.Count() != 2 || !headerFile[0][0].Equals("DatasetId"))
                    {
                        myFile.Message = new ErrorMessage() { Text = "ERROR: " + myFile.DatasetId + " Something is wrong with the structure of the header file" };
                        continue;
                    }

                    DateTime d1;

                    if (!DateTime.TryParseExact(headerFile[1][1], "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out d1))
                    {
                        myFile.Message = new ErrorMessage() { Text = "ERROR: " + myFile.DatasetId + " Date of update if header file missing or wrong format" };
                        continue;
                    }
                    else
                    {
                        oldUpdate = d1;
                    }
                }

                DateTime? newUpdate = null;
                DateTime d2;
                foreach (var line in csvUpdateFile)
                {
                    if (line[1].Trim('\"').Equals(myFile.DatasetId))
                    {
                        myFile.Description = line[0].Trim('\"').Trim(' ');

                        if (!DateTime.TryParseExact(line[3].Trim('\"'), "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d2))
                        {
                            myFile.Message = new ErrorMessage() { Text = "ERROR: " + myFile.DatasetId + " Date of update in eurostat file missing or wrong format" };
                            continue;
                        }
                        else
                        {
                            newUpdate = d2;
                        }
                    }
                }

                if (newUpdate == null)
                {
                    myFile.Message = new ErrorMessage() { Text = "ERROR: " + myFile.DatasetId + " This dataset was not found in Eurostat" };
                    myFile.Update = false;
                    continue;
                }

                if (newUpdate > oldUpdate)
                {
                    myFile.Update = true;
                    myFile.NewUpdateDate = newUpdate;
                    continue;
                }
                else
                {
                    myFile.Message = new ReturnMessage { Text = myFile.DatasetId + " was already up to date" };
                    myFile.Update = false;
                    continue;
                }
            }
        }


        static async Task SdmxFileConversionLoop()
        {
            var downloadTasks = new List<Task>();

            foreach (HeaderFile myFile in _filesToDownload)
            {
                if (myFile.Update)
                {
                    var downloadTask = Task.Run(() => ProcessingSdmxFile(myFile));
                    downloadTasks.Add(downloadTask);
                    await downloadTask.ContinueWith(t => Console.WriteLine(myFile.Message.Text));
                }
                else
                {
                    var downloadTask = Task.Run(() => { return myFile.Message.Text; });
                    downloadTasks.Add(downloadTask);
                    await downloadTask.ContinueWith(t => Console.WriteLine(myFile.Message.Text));
                }
            }

            await Task.Delay(1000);

            await Task.WhenAll(downloadTasks);          
        }

        static Message ProcessingSdmxFile(HeaderFile headerFile)
        {
            try
            {

                Downloader dn = new Downloader();
                Message messDown = dn.DownloadFile(headerFile.BulkDownloadZipFileName, headerFile.ZipFilePath, headerFile.FolderPath);

                if (messDown is ErrorMessage)
                {
                    headerFile.Message = messDown;
                    return messDown;
                }

                Message messUnzip = dn.UnzipFile(headerFile.SdmxXmlFilePath, headerFile.DsdXmlFilePath, headerFile.ZipFilePath, headerFile.FolderPath);

                if (messUnzip is ErrorMessage)
                {
                    headerFile.Message = messUnzip;
                    return messUnzip;
                }

                SdmxParser par = new SdmxParser(headerFile);
                //SdmxParser par = new SdmxParser(headerFile.FileName, headerFile.SdmxXmlFilePath, headerFile.DsdXmlFilePath, headerFile.FolderPath);

                Message messOk = new ReturnMessage() { Text = "File processed: " + headerFile.DatasetId };
                headerFile.Message = messOk;

            }
            catch (Exception ex)
            {
                Message messErr = new ErrorMessage() { Text = "ERROR : " + headerFile.DatasetId + " " + ex.Message  };
                headerFile.Message = messErr;
                return new ErrorMessage() { Text = "ERROR : " + headerFile.DatasetId + " " + ex.Message };
            }

            return new ReturnMessage() { Text = "ok" };
        }

        static Message SettingsFileReader(string[] args)
        {
            if (args == null || args.Length < 1)
            {
                return new ErrorMessage() { Text = "ERROR: FilesToDownload.txt file path not specified as input argument!" };
            }

            string settingsPath = args[0];

            if (!File.Exists(settingsPath))
            {
                return new ErrorMessage() { Text = "ERROR: FilesToDownload file does not exist: " + settingsPath };
            }

            if (!Path.GetExtension(settingsPath).Equals(".txt"))
            {
                return new ErrorMessage() { Text = "ERROR: FilesToDownload file should be .txt file" };
            }

            try
            {
                string[] lines = System.IO.File.ReadAllLines(settingsPath);

                if (lines.Length == 0)
                {
                    return new ErrorMessage() { Text = "ERROR: No datasets specified in FilesToDownload.txt file" };
                }
                
                foreach (var line in lines.Take(200))
                {
                    _filesToDownload.Add(new HeaderFile(line, AppDomain.CurrentDomain.BaseDirectory + @"\Data\"));
                }

            }
            catch (Exception ex)
            {
                return new ErrorMessage() { Text = ex.InnerException.Message };
            }

            return new ReturnMessage() { Text = "ok" };

        }        
    }
}
