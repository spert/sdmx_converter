﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxImportConsole
{
    public class CodeValue
    {
        string _datasetId;
        string _codelist;
        string _code;
        string _description;


        public string DatasetId { get { return _datasetId ; } set { _datasetId = value; } }

        public string CodeList { get { return _codelist ; } set { _codelist = value; } }

        public string Code { get { return _code ; } set { _code = value; } }

        public string Description { get { return _description ; } set { _description = value; } }
    }

    public sealed class CodeValuesMap : ClassMap<CodeValue>
    {
        public CodeValuesMap()
        {
            Map(m => m.DatasetId).ConvertUsing(m => m.DatasetId.InQuotes());
            Map(m => m.CodeList).ConvertUsing(m => m.CodeList.InQuotes());
            Map(m => m.Code).ConvertUsing(m => m.Code.InQuotes());
            Map(m => m.Description).ConvertUsing(m => m.Description.InQuotes());
        }
    }
}
