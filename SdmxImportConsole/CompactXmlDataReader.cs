﻿using CsvHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Linq;

namespace SdmxImportConsole
{
    public class CompactXmlDataReader : XmlDataReader
    {
        public CompactXmlDataReader(string SdmxFileName, string SdmxDataFilePath, string OutputFilePath, XmlDsdReader XmlDsdReader)
          : base(SdmxFileName, SdmxDataFilePath, OutputFilePath, XmlDsdReader)
        {

        }

        public override void ConvertAllSeries()
        {
            IEnumerable<Observation> observations = ReadObservations();

            string observationsFilePath = _outputFilePath + _sdmxFileName + "_data.csv";

            if (File.Exists(observationsFilePath))
            {
                File.Delete(observationsFilePath);
            }

            using (TextWriter writer = new StreamWriter(observationsFilePath, false, System.Text.Encoding.UTF8))
            {
                var csv = new CsvWriter(writer);
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = ";";
                csv.Configuration.ShouldQuote = (field, context) =>
                {
                    return false;
                };

                //Headers
                var dimensions = _xmlDsdReader.GetDimensions().Where(d => d.IsTimeDimension == 0 && d.IsPrimaryMeasure == 0 && d.IsObservationStatus == 0 && d.IsTimeFormat == 0);
                csv.WriteField("DATASET_ID");

                foreach (var header in dimensions)
                {
                    csv.WriteField(header.Codelist);
                }

                csv.WriteField("DATES");
                csv.WriteField("VALUES");                
                csv.WriteField("STATUS");
                csv.NextRecord();
                
                //Records
                csv.WriteRecords(observations);
            }
        }

        private IEnumerable<Observation> ReadObservations()
        {
            string timeDim = _xmlDsdReader.GetTimeDimension();
            string primMeas = _xmlDsdReader.GetPrimaryMeasure();
            string obsStatus = _xmlDsdReader.GetObsStatus();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            using (XmlReader reader = XmlReader.Create(_sdmxDataFilePath, settings))
            {
                reader.MoveToContent();

                string concat = "";
                string dateFormat = "";

                while (reader.Read())
                {
                    if (reader.IsStartElement() && reader.LocalName == "Series")
                    {
                        concat = "";
                        var dimensions = _xmlDsdReader.GetDimensions().Where(d => d.IsTimeDimension == 0 && d.IsPrimaryMeasure == 0 && d.IsObservationStatus == 0 && d.IsTimeFormat == 0);

                        foreach (var dim in dimensions)
                        {
                            var val = reader.GetAttribute(dim.ConceptRef);

                            if (val != null)
                                concat = concat + val.InQuotes() + ";";
                            else
                                concat = concat + "NA".InQuotes() + ";";
                        }

                        concat = concat.TrimEnd(';');

                        var format = _xmlDsdReader.GetDimensions().Where(x => x.IsTimeFormat == 1).FirstOrDefault();
                        if (format != null)
                        {
                            dateFormat = reader.GetAttribute(format.ConceptRef);
                        }
                    }

                    if (reader.IsStartElement() && reader.LocalName == "Obs")
                    {
                        Observation obs = new Observation();
                        obs.DatasetId = _sdmxFileName.InQuotes() ;
                        obs.Names = concat;

                        string primXAttr = reader.GetAttribute(primMeas);
                        if (primXAttr != null)
                        {
                            obs.Values = primXAttr;

                            if (primXAttr.Equals(":"))
                            {
                                int a;
                            }

                        }
                        else
                        {
                            obs.Values = "";

                            if (primXAttr != null && primXAttr.Equals(":"))
                            {
                                int a;
                            }
                        }

                        string timeXAttr = reader.GetAttribute(timeDim);
                        if (timeXAttr != null)
                        {
                            obs.Dates = Util.ConvertDateFormat(timeXAttr, dateFormat);
                        }

                        string obsStatusXAttr = reader.GetAttribute(obsStatus);
                        if (obsStatusXAttr != null)
                        {
                            obs.Status = obsStatusXAttr.InQuotes();
                        }
                        else
                        {
                            obs.Status = "";
                        }

                        yield return obs;

                    }
                }
            }

        }      
    }
}
