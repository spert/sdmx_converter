﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace SdmxImportConsole
{
    public class HeaderFile
    {
        string _rootFolderPath;

        public HeaderFile(string DatasetId, string RootFolderPath)
        {
            _datasetId = DatasetId;
            _rootFolderPath = RootFolderPath;

        }

        private string _datasetId;

        public string DatasetId
        {
            get { return _datasetId ; }
            set { _datasetId = value; }
        }

        private bool _update;

        public bool Update
        {
            get { return _update; }
            set { _update = value; }
        }

        private Message _mess;

        public Message Message
        {
            get { return _mess; }
            set { _mess = value; }
        }

        private DateTime? _newUpdateDate;

        public DateTime? NewUpdateDate
        {
            get { return _newUpdateDate; }
            set { _newUpdateDate = value; }
        }       

        public string LastUpdated
        {
            get
            {
                DateTime d;
                if (_newUpdateDate != null)
                {
                    d = _newUpdateDate.Value;

                    return d.ToString("yyyyMMdd");
                }

                return "";
            }
        }

        public string LastUpdated2
        {
            get
            {
                DateTime d;
                if (_newUpdateDate != null)
                {
                    d = _newUpdateDate.Value;

                    return d.ToString("dd.MM.yyyy");
                }

                return "";
            }
        }

        private string _description;

        public string Description
        {
            get { return _description  ; }
            set { _description = value; }
        }

        public string RootFolderPath
        {
            get { return _rootFolderPath + @"\"; }
        }

        public string FolderPath
        {
            get { return _rootFolderPath + @"\" + _datasetId + @"\"; }
        }

        public string HeaderFilePath
        {
            get { return _rootFolderPath + @"\" + _datasetId + @"\" + _datasetId + "_header.csv"; }
        }

        public string BulkDownloadZipFileName
        {
            get { return _datasetId + ".sdmx.zip"; }
        }

        public string ZipFilePath
        {
            get { return _rootFolderPath + @"\" + _datasetId + @"\" + _datasetId + @".sdmx.zip__" + LastUpdated2 + ".zip"; }
        }

        public string SdmxXmlFilePath
        {
            get { return _rootFolderPath + @"\" + _datasetId + @"\" + _datasetId + ".sdmx.xml"; }
        }

        public string DsdXmlFilePath
        {
            get { return _rootFolderPath + @"\" + _datasetId + @"\" + _datasetId + ".dsd.xml"; }
        }
    }

    public sealed class HeaderClassMap : ClassMap<HeaderFile>
    {
        public HeaderClassMap()
        {
            Map(m => m.DatasetId).ConvertUsing(m => m.DatasetId.InQuotes());
            Map(m => m.LastUpdated).ConvertUsing(m => m.LastUpdated);
            Map(m => m.Description).ConvertUsing(m => m.Description.InQuotes());
            Map(m => m.DsdXmlFilePath).Ignore();
            Map(m => m.SdmxXmlFilePath).Ignore();
            Map(m => m.ZipFilePath).Ignore();
            Map(m => m.BulkDownloadZipFileName).Ignore();
            Map(m => m.HeaderFilePath).Ignore();
            Map(m => m.FolderPath).Ignore();
            Map(m => m.RootFolderPath).Ignore();
            Map(m => m.NewUpdateDate).Ignore();
            Map(m => m.Message).Ignore();
            Map(m => m.Update).Ignore();
            Map(m => m.LastUpdated2).Ignore();
        }
    }      
}


