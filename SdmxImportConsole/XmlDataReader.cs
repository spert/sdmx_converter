﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Linq;

namespace SdmxImportConsole
{
    public abstract class XmlDataReader
    {
        protected string _sdmxFileName;

        protected string _sdmxDataFilePath;

        protected string _outputFilePath;

        protected XmlDsdReader _xmlDsdReader;


        public XmlDataReader(string SdmxFileName, string DataFilePath, string OutputFilePath, XmlDsdReader XmlDsdReader)
        {
            _sdmxFileName = SdmxFileName;
            _sdmxDataFilePath = DataFilePath;
            _outputFilePath = OutputFilePath;
            _xmlDsdReader = XmlDsdReader;

            string[] data = Path.GetFileNameWithoutExtension(_sdmxDataFilePath).Split('.');

            if (!File.Exists(_sdmxDataFilePath))
                throw new Exception("Data SDMX file" + _sdmxDataFilePath + "not found!");
            else if (!Path.GetExtension(_sdmxDataFilePath).Equals(".xml"))
                throw new Exception("File" + _sdmxDataFilePath + " is of wrong type!");
            else if (!data[data.Length - 1].ToLower().Equals("sdmx"))
                throw new Exception(".sdmx. not found in file name of " + _sdmxDataFilePath + ")");

            if (!Directory.Exists(Path.GetDirectoryName(_outputFilePath)))
                throw new Exception("Folder " + Path.GetDirectoryName(_outputFilePath) + "does not exist!");
       
         }        

        public abstract void ConvertAllSeries();

      
    }
}
