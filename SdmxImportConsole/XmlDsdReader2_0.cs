﻿using CsvHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace SdmxImportConsole
{
    public class XmlDSDReader2_0 : XmlDsdReader
    {
        List<Dimension> _dimensions = new List<Dimension>();        

        public XmlDSDReader2_0(string SdmxFileName, string DsdFilePath, string OutputFilePath) : base(SdmxFileName, DsdFilePath, OutputFilePath)
        {

        }

        public override bool ExtractDimensions()
        {
            IEnumerable<XElement> concepts = _xmlDsdDocument.Descendants()
                 .Where(x => x.Name.LocalName.Equals("ConceptScheme"))
                   .Elements().Where(x => x.Name.LocalName.Equals("Concept"));

            if (concepts != null)
            {
                Hashtable conc = new Hashtable();

                foreach (var c in concepts)
                {
                    string concId = c.Attribute("id").Value;
                    string concName = c.Descendants().First().Value;
                    conc.Add(concId, concName);
                }

                IEnumerable<XElement> compnents = _xmlDsdDocument.Descendants()
                    .Where(x => x.Name.LocalName.Equals("Components")).Elements();

                if (compnents != null)
                {
                    foreach (var d in compnents)
                    {
                        string structureName = d.Name.LocalName;                                               

                        int isPrimaryMeasure = 0;
                        if (structureName.Equals("PrimaryMeasure"))
                            isPrimaryMeasure = 1;

                        int isTimeDimension = 0;
                        if (structureName.Equals("TimeDimension"))
                            isTimeDimension = 1;
                                               
                        int isFrequencyDimension = 0;
                        XAttribute isfreqDimXElem = d.Attribute("isFrequencyDimension");
                        if (isfreqDimXElem != null)
                        {
                            var freqDimValue = isfreqDimXElem.Value;
                            if (freqDimValue.ToLower().Equals("true"))
                                isFrequencyDimension = 1;
                        }

                        string conceptRef = "";
                        XAttribute conceptRefXElem = d.Attribute("conceptRef");
                        if (conceptRefXElem != null)
                        {
                            conceptRef = conceptRefXElem.Value;
                        }

                        int isTimeFormat = 0;                        
                        if (conceptRefXElem != null && conceptRef.ToLower().Equals("time_format"))
                        {
                            isTimeFormat = 1;
                        }

                        int isObservationStatus = 0;
                        if (conceptRefXElem != null && conceptRef.ToLower().Equals("obs_status"))
                        {
                            isObservationStatus = 1;
                        }

                        string codeList = "";
                        XAttribute codeListXElem = d.Attribute("codelist");
                        if (codeListXElem != null)
                        {
                            codeList = codeListXElem.Value;
                        }
                        else
                        {
                            codeList = "NA";
                        }

                        string codeListAgency = "";
                        XAttribute codeListAgencyXElem = d.Attribute("codelistAgency");
                        if (codeListAgencyXElem != null)
                        {
                            codeListAgency = codeListAgencyXElem.Value;
                        }

                        string description = conc[conceptRef].ToString();

                        Dimension dim = new Dimension(_sdmxFileName, structureName, codeList, description, conceptRef, isTimeDimension,  isFrequencyDimension, isTimeFormat, isPrimaryMeasure, isObservationStatus);
                        _dimensions.Add(dim);
                    }
                }

            }

            return true;
        }

        public override List<Dimension> GetDimensions()
        {
            return _dimensions;
        }

        public override string GetPrimaryMeasure()
        {
            var pm = _dimensions.Where(x => x.IsPrimaryMeasure == 1).FirstOrDefault();

            if (pm == null)
                return null;
            else
                return pm.ConceptRef;
        }

        public override string GetTimeDimension()
        {
            var td = _dimensions.Where(x => x.IsTimeDimension == 1).FirstOrDefault();

            if (td == null)
                return null;
            else
                return td.ConceptRef;
        }

        public override string GetObsStatus()
        {
            var td = _dimensions.Where(x => x.IsObservationStatus == 1).FirstOrDefault();

            if (td == null)
                return null;
            else
                return td.ConceptRef;
        }

        public override bool SaveDimensionsToCsv()
        {
            string headerFilePath = _outputFilePath + _sdmxFileName + "_dimensions.csv";

            if (File.Exists(headerFilePath))
            {
                File.Delete(headerFilePath);
            }

            using (TextWriter writer = new StreamWriter(headerFilePath, false, System.Text.Encoding.UTF8))
            {
                var csv = new CsvWriter(writer);
                csv.Configuration.RegisterClassMap<DimensionClassMap>();
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = ";";
                csv.Configuration.ShouldQuote = (field, context) =>
                {
                    return false;
                };
                csv.WriteField("DATASET_ID");
                csv.WriteField("STRUCTURENAME");
                csv.WriteField("CODELIST");
                csv.WriteField("DESCRIPTION");
                csv.WriteField("CONCEPTREF");
                csv.WriteField("ISTIMEDIMENSION");
                csv.WriteField("ISFREQUENCYDIMENSION");
                csv.WriteField("ISTIMEFORMAT");
                csv.WriteField("ISPRIMARYMEASURE");
                csv.WriteField("ISOBSERVATIONSTATUS");
                csv.WriteField("ISTIMEFORMAT");
                csv.NextRecord();

                csv.WriteRecords(_dimensions);
            }

            return true;
        }


        public override bool SaveCodeListToCsv()
        {
            IEnumerable<CodeValue> codevalues = ReadCodeListValues();

            string codeListsFilePath = _outputFilePath + _sdmxFileName + "_codes.csv";

            if (File.Exists(codeListsFilePath))
            {
                File.Delete(codeListsFilePath);
            }

            using (TextWriter writer = new StreamWriter(codeListsFilePath, false, System.Text.Encoding.UTF8))
            {
                var csv = new CsvWriter(writer);
                csv.Configuration.RegisterClassMap<CodeValuesMap>();
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = ";";
                csv.Configuration.ShouldQuote = (field, context) =>
                {
                    return false;
                };

                csv.WriteField("DATASET_ID");
                csv.WriteField("CODELIST");
                csv.WriteField("CODE");
                csv.WriteField("DESCRIPTION");
                csv.NextRecord();

                csv.WriteRecords(codevalues);
            }

            return true;
        }

        private IEnumerable<CodeValue> ReadCodeListValues()
        {
            var codeLists = _xmlDsdDocument.Descendants().Where(x => x.Name.LocalName.Equals("CodeLists"));

            foreach (var codeList in codeLists.Descendants().Where(x => x.Name.LocalName.Equals("CodeList")))
            {
                string codeListId = "";

                XAttribute cat = codeList.Attribute("id");

                if (cat != null)
                {
                    codeListId = cat.Value;
                }
                //else
                //{
                //    codeListId = "NULL";
                //}

                foreach (var code in codeList.Descendants().Where(x => x.Name.LocalName.Equals("Code")))
                {
                    CodeValue c = new CodeValue();
                    c.DatasetId = _sdmxFileName;
                    c.CodeList = codeListId;

                    XAttribute at = code.Attribute("value");

                    if (at != null)
                    {
                        c.Code = at.Value;
                    }
                    //else
                    //{
                    //    c.Code = "NULL";
                    //}

                    XElement el = code.Descendants().FirstOrDefault();

                    if (el != null)
                    {
                        c.Description = el.Value;
                    }
                    //else
                    //{
                    //    c.Description = "NULL";
                    //}

                    yield return c;
                }
            }
        }
    }

}
   