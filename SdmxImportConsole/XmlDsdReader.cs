﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace SdmxImportConsole
{
    public abstract class XmlDsdReader
    {
        protected string _sdmxFileName;

        protected string _dsdFilePath;

        protected string _outputFilePath;

        protected XDocument _xmlDsdDocument;

        protected Hashtable _header = new Hashtable();
        
        public XmlDsdReader(string SdmxFileName, string DsdFilePath, string OutputFilePath)
        {
            _sdmxFileName = SdmxFileName;
            _dsdFilePath = DsdFilePath;
            _outputFilePath = OutputFilePath;

            string[] data = Path.GetFileNameWithoutExtension(_dsdFilePath).Split('.');

            if (!File.Exists(_dsdFilePath))
                throw new Exception("Data DSD file" + _dsdFilePath + "not found!");

            if (!Path.GetExtension(_dsdFilePath).Equals(".xml"))
                throw new Exception("File" + _dsdFilePath + " is of wrong type!");

            if (!data[data.Length - 1].ToLower().Equals("dsd"))
                throw new Exception(".dsd. not found in file name of " + _dsdFilePath + ")");

            if (!Directory.Exists(Path.GetDirectoryName(_outputFilePath)))
                throw new Exception("Folder " + Path.GetDirectoryName(_outputFilePath) + "does not exist!");

            try
            {
                _xmlDsdDocument = XDocument.Load(_dsdFilePath);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public abstract List<Dimension> GetDimensions();

        public abstract bool ExtractDimensions();

        public abstract bool SaveDimensionsToCsv();

        public abstract string GetPrimaryMeasure();

        public abstract string GetTimeDimension();

        public abstract string GetObsStatus();

        public abstract bool SaveCodeListToCsv();      
        

    }
}
