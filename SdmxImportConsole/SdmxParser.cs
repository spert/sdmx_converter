﻿using CsvHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace SdmxImportConsole
{
    public class SdmxParser
    {
        string _fileName;
        string _sdmxXmlFilePath;
        string _dsdXmlFilePath;
        string _folderPath;

        XmlDsdReader _xmlDsdReader;
        XmlDataReader _xmlDataReader;

        public SdmxParser(HeaderFile headerFile)
        {
            _sdmxXmlFilePath = headerFile.SdmxXmlFilePath;
            _dsdXmlFilePath = headerFile.DsdXmlFilePath;
            _folderPath = headerFile.FolderPath;
            _fileName = headerFile.DatasetId;

            InitializeSdmxDsdParser();

            _xmlDsdReader.ExtractDimensions();

            _xmlDataReader = new CompactXmlDataReader(_fileName, _sdmxXmlFilePath, _folderPath, _xmlDsdReader);

            _xmlDataReader.ConvertAllSeries();

            _xmlDsdReader.SaveDimensionsToCsv();

            _xmlDsdReader.SaveCodeListToCsv();

            SaveHeaderFile(headerFile);

            DeleteXmlFilesToSaveDiskSpace();

        }

        public void InitializeSdmxDsdParser()
        {
             _xmlDsdReader = new XmlDSDReader2_0(_fileName, _dsdXmlFilePath, _folderPath);
        }        

        public void SaveHeaderFile(HeaderFile headerFile)
        {
            List<HeaderFile> dto = new List<HeaderFile>();
            dto.Add(headerFile);

            if (File.Exists(headerFile.HeaderFilePath))
            {
                File.Delete(headerFile.HeaderFilePath);
            }

            using (TextWriter writer = new StreamWriter(headerFile.HeaderFilePath, false, System.Text.Encoding.UTF8))
            {
                var csv = new CsvWriter(writer);
                csv.Configuration.RegisterClassMap<HeaderClassMap>();
                csv.Configuration.HasHeaderRecord = true;
                csv.Configuration.Delimiter = ";";
                csv.Configuration.ShouldQuote = (field, context) =>
                {
                    return false;
                };

                csv.WriteRecords(dto);
            }
        }

        public void DeleteXmlFilesToSaveDiskSpace()
        {
            Directory.EnumerateFiles(_folderPath, "*.xml").ToList().ForEach(x => File.Delete(x));
        }









        //OK
        //public bool SearchFileSeries(string sdmxDataFilePath, string sdmxDataFileType, string vers, string searchExpression, ref Hashtable htAtt, ref Hashtable htAttCodes)
        //{
        //    XmlDataReader xmlDataReader = (XmlDataReader)this._sdmxDataReader[(object)sdmxDataFilePath.ToLower()];
        //    if (xmlDataReader == null)
        //    {
        //        xmlDataReader = XmlDataReader.InitializeXmlDataReader(sdmxDataFilePath, sdmxDataFileType, vers, this._dsdFile);
        //        if (xmlDataReader != null)
        //            this._sdmxDataReader.Add((object)sdmxDataFilePath.ToLower(), (object)xmlDataReader);
        //    }

        //    return xmlDataReader != null && xmlDataReader.ReadSeriesSelection(searchExpression, ref htAtt, (string)null);
        //}

        //OK
        //public bool ReadSeriesAttributesInFile(string id, ref object attr, ref object attrCodes)
        //{
        //    XmlDataReader readerFromObjectId = this.GetSdmxReaderFromObjectId(id);
        //    string str1 = (string)null;
        //    if (readerFromObjectId == null)
        //        return false;
        //    id = id.ToLower();

        //    // kas on korrektne
        //    string[] strArray = id.Split("__");
        //    bool flag = true;
        //    string vsKey = strArray[1];
        //    if (strArray[1].StartsWith("s"))
        //    {
        //        string str2 = strArray[1].TrimStart('s');

        //        double d;
        //        if (double.TryParse(str2, out d))
        //        {
        //            flag = false;
        //            vsKey = str2;
        //        }
        //    }
        //    if (((IEnumerable<string>)strArray).Count<string>() > 2)
        //        str1 = strArray[2];

        //    Hashtable seriesAttributeInfo = new Hashtable();
        //    Hashtable seriesAttributeInfoCodes = new Hashtable(); 

        //    XElement seriesObj = !flag ? readerFromObjectId.GetSeriesByIndex(checked(Conversions.ToInteger(vsKey) - 1)) : readerFromObjectId.GetSeriesByKey(vsKey);

        //    readerFromObjectId.ReadObjectAttributesMod(seriesObj, ref seriesAttributeInfo, ref seriesAttributeInfoCodes, false);

        //    if (str1 != null)
        //        seriesAttributeInfo.Add((object)"type", (object)"alpha");

        //    Util.ConvertHT_2_ObjectArray(ref attr, seriesAttributeInfo);
        //    Util.ConvertHT_2_ObjectArray(ref attrCodes, seriesAttributeInfoCodes);




        //    return true;
        //}

        //Ok
        //public bool ReadSeriesWithAttributes(string id, ref object attr, ref object vals, ref object ids, ref object cat, ref object codes)
        //{
        //    XmlDataReader readerFromObjectId = this.GetSdmxReaderFromObjectId(id);

        //    string seriesSuffixIdentifier = (string)null;
        //    id = id.ToLower();
        //    string[] strArray = id.Split("__");
        //    bool flag = true;
        //    string vsKey = strArray[1];
        //    if (strArray[1].StartsWith("s"))
        //    {
        //        string str = strArray[1].TrimStart('s');

        //        double d;
        //        if (double.TryParse(str, out d))
        //        {
        //            flag = false;
        //            vsKey = str;
        //        }
        //    }

        //    if (((IEnumerable<string>)strArray).Count<string>() > 2)
        //        seriesSuffixIdentifier = strArray[2];

        //    if (readerFromObjectId != null)
        //    {
        //        XElement seriesObj = !flag ? readerFromObjectId.GetSeriesByIndex(checked(Conversions.ToInteger(vsKey) - 1)) : readerFromObjectId.GetSeriesByKey(vsKey);
        //        if (seriesObj != null)
        //        {
        //            List<KeyValuePair<string, string>> seriesObservationInfo = new List<KeyValuePair<string, string>>();
        //            Hashtable seriesAttributeInfo = new Hashtable();
        //            Hashtable seriesCodesInfo = new Hashtable();
        //            readerFromObjectId.ReadObject(seriesObj, ref seriesAttributeInfo, ref seriesObservationInfo, ref seriesCodesInfo, seriesSuffixIdentifier);
        //            Util.ConvertHT_2_ObjectArray(ref attr, seriesAttributeInfo);
        //            Util.ConvertHT_2_ObjectArray(ref seriesObservationInfo, ref vals, ref ids);
        //            Util.ConvertHT_2_ObjectArray(ref codes, seriesCodesInfo);

        //            Hashtable att = new Hashtable();
        //            readerFromObjectId.ReadConcepts(ref att);
        //            Util.ConvertHT_2_ObjectArray(ref cat, att);

        //            return true;
        //        }
        //    }          

        //        return false;
        //}

        //public XmlDataReader GetSdmxReaderFromObjectId(string objectId)
        //{
        //    objectId.IndexOf("__");

        //    var xxx = (this._sdmxDataFilePath + objectId.Split("__")[0] + ".xml");

        //    return (XmlDataReader)this._sdmxDataReader[(object)(this._sdmxDataFilePath + objectId.Split("__")[0] + ".xml").ToLower()];
        //}
    }
}
