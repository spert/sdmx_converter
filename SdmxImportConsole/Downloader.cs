﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SdmxImportConsole
{
    class Downloader
    {

        public Message DownloadFile(string BulkDownloadSdmxZipFileName, string ZipFilePath, string RootFolder)
        {   
            try
            {
                if (!Directory.Exists(RootFolder))
                    Directory.CreateDirectory(RootFolder);

//#if DEBUG
                if (File.Exists(ZipFilePath))
                    File.Delete(ZipFilePath);
//#else
//                                   if (File.Exists(ZipFilePath))
//                                    return new ErrorMessage() { Text = "ERROR: File " + BulkDownloadSdmxZipFileName + " already exists" };
                                    
//#endif

                string url = @"https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&file=data/";


                using (WebClient wc = new WebClient())
                {
                    wc.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    wc.DownloadFile(new Uri(string.Concat(url, BulkDownloadSdmxZipFileName)), ZipFilePath);
                }

            }
            catch (Exception ex)
            {
                return new ErrorMessage() { Text = "ERROR: " + ex.Message.ToString() };
                }

            return new ReturnMessage() { Text = ZipFilePath };
        }


        public Message UnzipFile(string SdmxXmlFileName, string DsdXmlFileName, string ZipFilePath, string FolderPath)
        {
            try
            {
                if (File.Exists(SdmxXmlFileName))
                    File.Delete(SdmxXmlFileName);                

                if (File.Exists(DsdXmlFileName))                
                    File.Delete(DsdXmlFileName);
                
                FastZip fastZip = new FastZip();
                fastZip.ExtractZip(ZipFilePath, FolderPath, null);

            }
            catch (Exception ex)
            {
                return new ErrorMessage() { Text = "ERROR: " + ex.Message.ToString() };
                }

            return new ReturnMessage() { Text = null };
        }


        public static Message DownloadUpdatesFile()
        {

//#if (DEBUG)

//            return new ReturnMessage() { Text = @"C:\\Users\\kasutaja\\AppData\\Local\\Temp\\c6df8b3e-ce74-4c3e-9987-5b421e4dfd4e.txt" };

//#else           

            string tmpPath = Path.GetTempPath() + string.Format(@"{0}.txt", Guid.NewGuid());

            try
            {
                string url = @"https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&file=table_of_contents_en.txt";

                using (WebClient wc = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                    //wc.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    wc.DownloadFile(new Uri(url), tmpPath);
                }

                //using (HttpClient client = new HttpClient())
                //{
                //    //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;

                //    //    //wc.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                //    //    wc.DownloadFile(new Uri(url), tmpPath);
                //    //}
                //}
            }
            catch (Exception ex)
            {
                return new ErrorMessage() { Text = "ERROR: " + ex.Message.ToString() };
            }

            return new ReturnMessage() { Text = tmpPath };

//#endif

        }




        //private async Task DownloadMultipleFilesAsync(List<DocumentObject> doclist)
        //{
        //    await Task.WhenAll(doclist.Select(doc => DownloadFileAsync(doc)));
        //}



        //private void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        //{
        //    if (e.Cancelled)
        //    {
        //        Console.WriteLine("The download has been cancelled");
        //        return;
        //    }

        //    if (e.Error != null) 
        //    {
        //        Console.WriteLine("An error ocurred while trying to download file");

        //        return;
        //    }

        //    Console.WriteLine("File succesfully downloaded");
        //}


        //public async Task<Message> DownloadFileAsync(string FileName, string FilePath, string Folder)
        //{
        //    try
        //    {
        //        if (!Directory.Exists(Folder))
        //            Directory.CreateDirectory(Folder);

        //        if (File.Exists(FilePath))
        //            return new ErrorMessage() { Text = "File " + FileName + " already exists" };

        //        string url = @"https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&file=data/";

        //        using (WebClient webClient = new WebClient())
        //        {
        //            webClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
        //            await webClient.DownloadFileTaskAsync(new Uri(string.Concat(url, FileName)), FilePath);

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return new ErrorMessage() { Text = ex.InnerException.Message };
        //    }

        //    return new ReturnMessage() { Text = FilePath };
        //}


    }
}
