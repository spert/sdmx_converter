﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace SdmxImportConsole
{
    public class Observation
    {
        string _datasetId;
        string _obsAlias;
        string _obsDate;
        string _obsValue;
        string _obsStatus;

        public Observation()
        {

        }

        //public Observation(string DatasetId, string ObsAlias, string ObsDate, string ObsValue, string ObsStatus)
        //{
        //    _datasetId = DatasetId;
        //    _obsAlias = ObsAlias;
        //    _obsDate = ObsDate;
        //    _obsValue = ObsValue;
        //    _obsStatus = ObsStatus;
        //}

        public string DatasetId { get { return _datasetId; } set { _datasetId = value; } }

        public string Names { get { return _obsAlias; } set { _obsAlias = value; } }

        public string Dates { get { return _obsDate; } set { _obsDate = value; } }

        public string Values { get { return _obsValue; } set { _obsValue = value; } }

        public string Status { get { return _obsStatus; } set { _obsStatus = value; } }

    }
    
    //public sealed class ObservationClassMap : ClassMap<Observation>
    //{
    //    public ObservationClassMap()
    //    {
    //        Map(m => m.DatasetId);//.ConvertUsing(m => $"\"{m.DatasetId}\"");
    //        Map(m => m.Names);//.ConvertUsing(m => $"\"{m.Names}\"");
    //        Map(m => m.Dates);//.ConvertUsing(m => m.Dates);
    //        Map(m => m.Values); // '.ConvertUsing(m => m.Values);
    //        Map(m => m.Status); //.ConvertUsing(m => m.Status != null ?  $"\"{m.Status}\"" : null);
    //    }
    //}
}
