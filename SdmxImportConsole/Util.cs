﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;

namespace SdmxImportConsole
{
    public class Util
    {
        public static bool CategoryBrowserIsClosing = false;

        public static string ConvertDateFormat(string MyDate, string MyDateFormat)
        {
            //P1D,
            //P7D,
            //P1M,
            //P3M,
            //P6M,
            //P1Y,
            //PT1M,


            if (MyDateFormat.Equals("P3M"))
            {
                if (MyDate.EndsWith("-Q1"))
                    return string.Concat(MyDate.Substring(0, 4), "0331");
                else if (MyDate.EndsWith("-Q2"))
                    return string.Concat(MyDate.Substring(0, 4), "0630");
                else if (MyDate.EndsWith("-Q3"))
                    return string.Concat(MyDate.Substring(0, 4), "0930");
                else if (MyDate.EndsWith("-Q4"))
                    return string.Concat(MyDate.Substring(0, 4), "1231");
            }

            if (MyDateFormat.Equals("P1M"))
            {
                if (MyDate.EndsWith("-Q1"))
                    return string.Concat(MyDate.Substring(0, 4), "0331");
                else if (MyDate.EndsWith("-Q2"))
                    return string.Concat(MyDate.Substring(0, 4), "0630");
                else if (MyDate.EndsWith("-Q3"))
                    return string.Concat(MyDate.Substring(0, 4), "0930");
                else if (MyDate.EndsWith("-Q4"))
                    return string.Concat(MyDate.Substring(0, 4), "1231");

                if (MyDate.EndsWith("-01"))
                    return string.Concat(MyDate.Substring(0, 4), "0131");
                else if (MyDate.EndsWith("-02"))
                    return FebruaryEOM(MyDate);
                else if (MyDate.EndsWith("-03"))
                    return string.Concat(MyDate.Substring(0, 4), "0331");
                else if (MyDate.EndsWith("-04"))
                    return string.Concat(MyDate.Substring(0, 4), "0430");
                else if (MyDate.EndsWith("-05"))
                    return string.Concat(MyDate.Substring(0, 4), "0531");
                else if (MyDate.EndsWith("-06"))
                    return string.Concat(MyDate.Substring(0, 4), "0630");
                else if (MyDate.EndsWith("-07"))
                    return string.Concat(MyDate.Substring(0, 4), "0731");
                else if (MyDate.EndsWith("-08"))
                    return string.Concat(MyDate.Substring(0, 4), "0831");
                else if (MyDate.EndsWith("-09"))
                    return string.Concat(MyDate.Substring(0, 4), "0930");
                else if (MyDate.EndsWith("-10"))
                    return string.Concat(MyDate.Substring(0, 4), "1031");
                else if (MyDate.EndsWith("-11"))
                    return string.Concat(MyDate.Substring(0, 4), "1130");
                else if (MyDate.EndsWith("-12"))
                    return string.Concat(MyDate.Substring(0, 4), "1231");

                return "null";
            }

            if (MyDateFormat.Equals("P1Y"))
            {
                return string.Concat(MyDate.Substring(0, 4), "1231");
            }

            return MyDate;
        }

        private static string FebruaryEOM(string MyDate)
        {
            if (DateTime.IsLeapYear(int.Parse(MyDate.Substring(0, 4))))
                return string.Concat(MyDate.Substring(0, 4), "0229");
            else
                return string.Concat(MyDate.Substring(0, 4), "0228");
        }
    }

    public static class Extensions
    {
        public static string InQuotes(this string inp)
        {
            return inp.Contains(";") ? "\""  + inp.Replace("\"", "\"\"\"")  + "\"" : inp.Replace("\"", "\"\"\"");
        }
    }

}
