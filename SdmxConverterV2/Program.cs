﻿
using SdmxConverterV2.Job;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Diagnostics;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using SdmxConverterV2.Util;
using Microsoft.Extensions.Configuration;

class Program
{
    public static IConfiguration Configuration { get; }

    static int Main(string[] args)
    {

        IConfiguration configuration = new ConfigurationBuilder()
        .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)        
        .AddEnvironmentVariables()
        .AddCommandLine(args)
        .Build();
              

        IHost _host = Host.CreateDefaultBuilder().ConfigureServices(
            services => {
                services.AddSingleton<IDbService, DbService>();
                services.AddSingleton<IFileManager, FileManager>();
            })
            .Build();


        var app = _host.Services.GetRequiredService<IDbService>();
        app.Run();

        //Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);

        //if (args.Length == 0)
        //{
        //    var sett = AppDomain.CurrentDomain.BaseDirectory + @"FilesToDownload.txt";

        //    if (File.Exists(sett))
        //    {
        //        args = new string[1];
        //        args[0] = AppDomain.CurrentDomain.BaseDirectory + @"FilesToDownload.txt";
        //    }
        //}

        //Database db = new Database();
        
        //for (int i = 0; i < 10; i++)
        //{
        //    Dataset ds = new Dataset();
        //    ds.DatasetId = "aa" + i.ToString();
        //    ds.DoUdpate = "true";
        //    ds.LastUpdated = DateTime.Now;  
        //    ds.Description = "Description";
        //    db.Datasets.Add(ds);
        //}

        //string file = @"C:\\Users\\kasutaja\\source\\repos\\sdmx_converter\\SdmxConverterV2\\bin\\Debug\\net6.0\\json.txt";

        //string jsonString = JsonSerializer.Serialize(db, new JsonSerializerOptions { WriteIndented = true });
        //File.WriteAllText(file, jsonString);

        //using FileStream openStream = File.OpenRead(file);
        //Database? dbx = JsonSerializer.Deserialize<Database>(jsonString);




        return 0;

    }
}